# Dynamic Malware Evaluation Framework (DMEF)#

DMEF is a framework for secure malware execution and analysis in scalable virtualized network environments.
DMEF provides a training and analysis environment for network administrators and researchers in the fight against malware.


